# Create your views here.
from django.http.response import HttpResponse
from django.shortcuts import render
from data.models import Book 

def search_form(request):
    return render(request, 'data/search_form.html')

def search(request):
    if 'q' in request.GET:
        message = 'You searched for: %r' % request.GET['q']
        books = Book.objects.filter(title__icontains=message)
        return render(request, 'data/search_results.html',
            {'books': books, 'query': message})
    else:
        message = 'You submitted an empty form.'
#         return HttpResponse(message)
        return render(request, 'data/search_form.html', {'error':True})
