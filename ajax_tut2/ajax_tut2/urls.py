from django.conf.urls.defaults import patterns, include, url
 
urlpatterns = patterns('',
    url(r'^ajaxexample$', 'ajax_tut2.views.main'),
    url(r'^ajaxexample_json$', 'ajax_tut2.views.ajax'),
)