from django.http import *
from django.shortcuts import render_to_response
from django.template import RequestContext
from django.utils import simplejson
import socket
 
def main(request):
   return render_to_response('ajaxexample.html', context_instance=RequestContext(request))

def ajax(request):
   if request.POST.has_key('client_response'):
        x = request.POST['client_response']
        # instead of get the localhost url, which is: HttpRequest.get_host(request)                  
        y = socket.gethostbyname(x)                           
        response_dict = {}
        response_dict.update({'server_response': y })                                                                   
        return HttpResponse(simplejson.dumps(response_dict), mimetype='application/javascript') 
   else:
        print "else first"
        return render_to_response('ajaxexample.html', context_instance=RequestContext(request))
