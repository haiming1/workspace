'''
Sometimes, MAS API won't work because the json won't be parsed, in this case, you can either use the local cache
or try another query
'''

from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.http import HttpResponse
from django.shortcuts import render_to_response
from django.template.context import RequestContext
from django.utils import simplejson
import re
import threading

import networkx as nx
from xmodel.DataAdaptor import DataAdaptor
from xutil.ProcessFeature import get_feature_set, construct_lda, make_clique
from xutil.clf import Classifier
from xutil.preprocess import cleanText

from time import time, sleep
import logging
logging.basicConfig(filename='ps2/log/%.f.log'%time(), level=logging.INFO)
logging.info('In the views.py')
# query specific paper_list and pagination, using global variable as the shared variable
# the lists order corresponds to each other
query = ''
paper_list = [] # the returned paper list given a query, # = endInd - startInd + 1
paper_dict = {} # paper_list and paper_dict are global variable for storing items related with current query
paper_texts = []
paper_authorID_list = []
paper_feature_set = [] # the feature set of the current paper list, under the current query, not preprocessed yet

# setup config
perpage = 10
paginator = Paginator(paper_list, perpage)
startInd = 1
endInd = 50 # API no limits
maxVisitedPage = 1 # of the current paper_list, the maximum page reached

# task specific variables
query_result = {} # store query and its ee-ss+1 results
# all_paper_dict = {} # not necessary for now
# not necessary for now, used for creating sna networks, need to uncomment in the search() function
all_paper_list = [] # maintained for each task, used for task specific features, such as lda and sna
all_paper_texts = [] # preprocessed stem texts for all_paper, e.g. [["hello", "world"],["adapt"]]
lda_model = None
all_paper_feature_dict = {} # ms_id: paper feature
selected_paper_dict = {} # user liked papers
# used to eliminate uninterested papers when generating
# but will be still present when using querying again
unselected_paper_dict = {} 
G = nx.Graph() # graph of co-authorship

constructing_flg = False
class Get_feature_Thread (threading.Thread):
    '''
    another thread for getting feature set of result set
    '''
    def __init__(self):
        threading.Thread.__init__(self)
    def run(self):
        global constructing_flg
        constructing_flg = True
        global paper_feature_set, lda_model, paper_list, paper_texts, paper_authorID_list, G
        global all_paper_feature_dict, all_paper_texts, query_result, query
        # if query is new, populate a bunch of data:
        if query not in query_result.keys():
            paper_texts = [] # list of each paper's text list
            paper_authorID_list = [] # list of each paper's author id list
            for paper in paper_list:
                paper_texts.append(cleanText(paper.title+' '+paper.title+' '+paper.abstract))
                paper_authorID_list.append(paper.author_id_list)
                G.add_edges_from(make_clique(paper.author_id_list))
            all_paper_texts += paper_texts
            papers_list_texts = {'paper_list':paper_list,
                         'paper_texts':paper_texts,
                         'paper_authorID_list':paper_authorID_list}
            query_result[query] = papers_list_texts
        # update lda model of this task
        # eliminate the query keys to avoid lda topics focusing on the few terms
        st = time()
        logging.info('----lda construction begins')
        lda_model = construct_lda(all_paper_texts, query_result.keys())
        logging.info('----lda construction finishes: %.2fs'%(time()-st))
        paper_feature_set = get_feature_set(lda_model, 
                                            paper_list, 
                                            paper_texts, 
                                            paper_authorID_list, G) # row: item, col: feature
        # populating all_paper_feature_dict
        tmp = 0
        for paper in paper_list:
            all_paper_feature_dict[paper.ms_id] = paper_feature_set[tmp]
            tmp += 1
        constructing_flg = False

def search(request):
    errors = {'no_query':'',
              'no_result':'',
              'no_selection':'',
              'empty':''
              }
    global paper_list, paper_texts, paper_authorID_list, paper_dict, query, paper_feature_set
    global paginator, startInd, endInd, perpage, maxVisitedPage
    global query_result, all_paper_list, all_paper_texts, lda_model, all_paper_feature_dict, G 
    global unselected_paper_dict, selected_paper_dict, constructing_flg

    if ('q' in request.GET and request.GET.get('q')!='') or 'page' in request.GET or \
                                            'g' in request.GET or 'e' in request.GET:
        # directed to search(request) via empty
        if 'e' in request.GET:
            logging.info('************************************************')
            logging.info('*                                              *')
            logging.info('*                                              *')
            logging.info('*                Starting Over                 *')
            logging.info('*                                              *')
            logging.info('*                                              *')
            logging.info('************************************************')
            # initialize everything
            query = ''
            paper_list = [] # the returned paper list given a query, # = endInd - startInd + 1
            paper_dict = {}
            paper_texts = []
            paper_authorID_list = []
            paper_feature_set = []
            
            paginator = Paginator(paper_list, perpage)
            maxVisitedPage = 1
            
            all_paper_list = []
            # query result will be used to train topic model
            # so each task should have different query result, since their topics are different
            query_result = {}
#             nx.write_gpickle(G,"test.gpickle") # for debugging purposes
            G.clear()
            all_paper_texts = []
            lda_model = None
            all_paper_feature_dict = {}
            selected_paper_dict = {}
            unselected_paper_dict = {}
            errors['empty'] = 'Please submit a query'

        # directed to search(request) via generate
        if 'g' in request.GET:
            if len(paper_list) > 0:
                if len(selected_paper_dict)<1 and len(unselected_paper_dict)<1: # if only like or dislike, assume there's at least unsure class out there
                    errors['no_selection'] = 'Please like or dislike at least 1 paper'
                else: 
                    st = time()
                    logging.info('--reranking starts')
                    while constructing_flg == True:
                        print "waiting for construction"
                        sleep(1) # while construting feature, wait for it
                    generate_new() # generate new paper_list and paper_dict
                    logging.info('--reranking finishes: %.2fs'%(time()-st))
                    maxVisitedPage = 1 # need to reset maxVisitedPage since paper_list is new
                    paginator = Paginator(paper_list, perpage) # put new papers into the paginator
            else:
                errors['no_query'] = 'Please submit a valid query before reranking!'
                return render_to_response('result.html', {'errors': errors.values()}, \
                                          context_instance=RequestContext(request))
        
        # directed to search(request) via query
        if 'q' in request.GET:
            query = request.GET.get('q')
            logging.info('--new query: %s' % query)
            query = " ".join(re.sub('[^0-9a-zA-Z]',' ', query).split())
            # if haven't been searched yet, we will start from new
            if query not in query_result.keys():
                logging.info('----this is a new query')
                da = DataAdaptor(query, startInd, endInd)
                paper_list = da.paper_list
                paper_dict = da.paper_dict
            else: # we can simply use this as cache
                logging.info('----this is an old query')
                paper_list = query_result[query]['paper_list']
                paper_dict = list2dict(paper_list)
                paper_texts = query_result[query]['paper_texts']
                paper_authorID_list = query_result[query]['paper_authorID_list']
                
            if paper_list == []:
                errors['no_result'] = 'No results found, please change query terms'
                return render_to_response('result.html', {'errors': errors.values()}, \
                                          context_instance=RequestContext(request))
            st = time()
            logging.info('--getting features of result set begins')
            thread2 = Get_feature_Thread()
            thread2.start()
            logging.info('--getting features of result set finishes: %.2f'%(time()-st))
                
            maxVisitedPage = 1 # need to reset maxVisitedPage since paper_list is new
            paginator = Paginator(paper_list, perpage)
            
        # directed to search(request) via page
        if 'page' in request.GET:
            page = request.GET.get('page')
        else:
            # if comes from other sources, will stay in the maxVisitedPage, 
            # even from generating without any selections to avoid going back to 1st page
            page = str(maxVisitedPage)
        try:
            papers = paginator.page(page)
            if int(page)>maxVisitedPage: maxVisitedPage = int(page)
        except PageNotAnInteger:
            # if page is not an integer, return the first page
            papers = paginator.page(1)
        except EmptyPage:
            # if page is out of range, return the last page
            papers = paginator.page(paginator.num_pages)
            maxVisitedPage = int(paginator.num_pages)
            
        # should not use post or even ajax post, or the page won't refresh, just regular get methods
        return render_to_response('result.html', {'errors':errors.values(),'papers':papers}, \
                                  context_instance=RequestContext(request))
    else:
        errors['no_query'] = 'Please submit a query'
        return render_to_response('result.html', {'errors': errors.values()}, \
                                  context_instance=RequestContext(request))

def test(request):
    return HttpResponse('hello world')

# @csrf_exempt
# show liked/selected or disliked/unselected papers
def show_pref(request):
    global selected_paper_dict, unselected_paper_dict
    pref_paper = {} # which is json to the success() in ajax function
    for ms_id, paper in selected_paper_dict.items():
        pref_paper['like:'+str(ms_id)] = paper.to_JSON()
    for ms_id, paper in unselected_paper_dict.items():
        pref_paper['dislike:'+str(ms_id)] = paper.to_JSON()
    return HttpResponse(simplejson.dumps(pref_paper), mimetype='application/javascript')

def update_pref(request):
    global selected_paper_dict, unselected_paper_dict
    if request.POST.has_key('pref_msid'):
        key = request.POST['pref_msid']
        pref_type = key.split()[0]
        msid = int(key.split()[1])
        if pref_type == 'Like':
            unselected_paper_dict.pop(msid, None)
            selected_paper_dict[msid] = paper_dict[msid]
        elif pref_type == "Dislike":
            selected_paper_dict.pop(msid, None)
            unselected_paper_dict[msid] = paper_dict[msid]
        else:
            unselected_paper_dict.pop(msid, None)
            selected_paper_dict.pop(msid, None)
        return HttpResponse(simplejson.dumps({}), mimetype='application/javascript')
    else:
        print "has no paperID..."

def generate_new(): 
    '''
        3-class classification, inherently supported by scipy
        risky problem:
        1. when loading from local cache, the 200 is duplicate, thus generating is erronous
        2. feature construction is another thread, generate must wait for it to finish.
    '''
    logging.info('----in the reranking')
    global query, selected_paper_dict, unselected_paper_dict, maxVisitedPage, perpage, all_paper_feature_dict # stay unchanged
    global paper_list, paper_texts, paper_authorID_list, paper_feature_set # will be updated
    
    # training set is current query set plus selected paper set and unselected paper set
    train_feature_set = paper_feature_set[:maxVisitedPage*perpage]
    # add already selected papers to the train set
    for ms_id in selected_paper_dict.keys():
        train_feature_set.append(all_paper_feature_dict[ms_id])
    # add already unselected papers to the train set
    for ms_id in unselected_paper_dict.keys():
        train_feature_set.append(all_paper_feature_dict[ms_id])

    label_list = []
    for paper in paper_list[:maxVisitedPage*perpage]:
        if paper.ms_id in selected_paper_dict.keys(): label_list.append(0)
        elif paper.ms_id in unselected_paper_dict.keys(): label_list.append(2)
        else: label_list.append(1)
    # add label of selected/unselected papers to label list
    label_list += [0]*len(selected_paper_dict.keys())
    label_list += [2]*len(unselected_paper_dict.keys())
    
    st = time()
    logging.info('----classifiers begins training')
    clf = Classifier(train_feature_set, label_list,
                                         ['like', 'unsure','dislike'], '3 class classifier',
                                         query) # 0 label comes first
    logging.info('----classifiers finishes training: %.2fs'%(time()-st))
    print "classifier trained"

    test_feature_set = paper_feature_set
    print "test feature set"
    print test_feature_set
    st = time()
    logging.info('----classifications begin')
    test_label_list = clf.clf(test_feature_set)
    logging.info('----classifications finish: %.2fs'%(time()-st))
    print "query results classified"
    
    liked_ind_list = [] # index of paper in paper_list actually liked
    prd_liked_ind_list = [] # index of paper predicted to be liked
    prd_unsure_ind_list = []
    prd_disliked_ind_list = []
    disliked_ind_list = []

    ind = 0
    for paper in paper_list:
        if paper.ms_id in selected_paper_dict.keys(): liked_ind_list.append(ind)
        elif paper.ms_id in unselected_paper_dict.keys(): disliked_ind_list.append(ind)
        else:
            if test_label_list[ind]==0:
                prd_liked_ind_list.append(ind) # predictions, 1st like, 2nd LikeUnsure -- like
            elif test_label_list[ind]==1:
                prd_unsure_ind_list.append(ind) # predictions, 1st like, 2nd dislike -- controversial
            elif test_label_list[ind]==2:
                prd_disliked_ind_list.append(ind) # predictions, 1st dislikeUnsure, 2nd LikeUnsure -- unsure
        ind += 1
    assert(ind == len(paper_list))

    new_paper_list = []
    new_paper_texts = []
    new_paper_authorID_list = []
    new_feature_set = []

    ind = 0
    while ind < len(liked_ind_list):
        new_paper_list.append(paper_list[liked_ind_list[ind]])
        new_paper_texts.append(paper_texts[liked_ind_list[ind]])
        new_paper_authorID_list.append(paper_authorID_list[liked_ind_list[ind]])
        new_feature_set.append(paper_feature_set[liked_ind_list[ind]])
        ind+=1
    ind = 0
    while ind < len(prd_liked_ind_list):
        new_paper_list.append(paper_list[prd_liked_ind_list[ind]])
        new_paper_texts.append(paper_texts[prd_liked_ind_list[ind]])
        new_paper_authorID_list.append(paper_authorID_list[prd_liked_ind_list[ind]])
        new_feature_set.append(paper_feature_set[prd_liked_ind_list[ind]])
        ind+=1
    ind = 0
    while ind < len(prd_unsure_ind_list):
        new_paper_list.append(paper_list[prd_unsure_ind_list[ind]])
        new_paper_texts.append(paper_texts[prd_unsure_ind_list[ind]])
        new_paper_authorID_list.append(paper_authorID_list[prd_unsure_ind_list[ind]])
        new_feature_set.append(paper_feature_set[prd_unsure_ind_list[ind]])
        ind+=1
    ind = 0
    while ind < len(prd_disliked_ind_list):
        new_paper_list.append(paper_list[prd_disliked_ind_list[ind]])
        new_paper_texts.append(paper_texts[prd_disliked_ind_list[ind]])
        new_paper_authorID_list.append(paper_authorID_list[prd_disliked_ind_list[ind]])
        new_feature_set.append(paper_feature_set[prd_disliked_ind_list[ind]])
        ind+=1
    ind = 0
    while ind < len(disliked_ind_list):
        new_paper_list.append(paper_list[disliked_ind_list[ind]])
        new_paper_texts.append(paper_texts[disliked_ind_list[ind]])
        new_paper_authorID_list.append(paper_authorID_list[disliked_ind_list[ind]])
        new_feature_set.append(paper_feature_set[disliked_ind_list[ind]])
        ind+=1
    assert(len(new_paper_list)==len(paper_list))
    assert(len(new_paper_texts) == len(paper_texts))
    assert(len(new_paper_authorID_list)==len(paper_authorID_list))
    assert(len(new_feature_set)==len(paper_feature_set))

    paper_list = new_paper_list
    paper_texts = new_paper_texts
    paper_authorID_list = new_paper_authorID_list
    paper_feature_set = new_feature_set

    print "updated global list with new values"
    return

def list2dict(paper_list):
    '''not only populate self.paper_dict, but also serve as a util function'''
    paper_dict = {}
    for paper in paper_list:
        paper_dict[paper.ms_id] = paper
    return paper_dict
    
def dict2list(paper_dict):
    '''serve as a util function'''
    paper_list = []
    for paper in paper_dict.values(): paper_list.append(paper)
    return paper_list
