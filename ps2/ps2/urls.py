from django.conf.urls import patterns, url

from ps2.views import search, show_pref, update_pref

urlpatterns = patterns('',
    url(r'^$', search),
    url(r'^pref/$', show_pref),
    url(r'^update_pref/$', update_pref),
)