'''
Created on 2013-10-28

@author: whm
'''
from datetime import date
import json


class PaperEntry(object):
    '''
    classdocs
    '''
    def __init__(self, id, citation_count, reference_count, year, title, abstract, author_list, 
                 author_id_list, ms_id, conf, jnl, v_id, kws, url):
        self.id = id # id is the sequence it shows in the query result, starting from startInd, different from its ms_id
        self.citation_count = citation_count
        self.reference_count = reference_count
        self.year = year
        self.recency = date.today().year - year
        self.title = title
        self.abstract = abstract
        self.author_list = author_list
        self.author_id_list = author_id_list
        self.ms_id = ms_id
        self.occured = False #  selected paper occur in the paper list
        self.conf = conf
        self.jnl = jnl
        self.v_id = v_id
        self.kws = kws # keywords
        self.url = url
    
    def printPaper(self):
        print '''Index:%d\n
                 Title:%s\n
                 Author_list:%s\n
                 Abstract:%s\n
                 Year:%d\n
                 ReferenceCount:%d\n
                 CitationCount:%d\n
                 ms_id:%d\n''' % (self.id, self.title, self.author_list, 
                                          self.abstract, self.year, self.reference_count, 
                                          self.citation_count, self.ms_id)
    
    def setId(self, id):
        self.id = id
        
    # ref:
    # http://stackoverflow.com/questions/3768895/python-how-to-make-a-class-json-serializable
    def to_JSON(self):
        return json.dumps(self, default=lambda o: o.__dict__, sort_keys=True, indent=4)
