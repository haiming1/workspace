'''
Created on 2013-10-28

@author: whm
'''
import re
import simplejson
from simplejson.decoder import JSONDecodeError
import sys
import urllib

from xmodel.PaperEntry import PaperEntry
from xutil.connection import internet_on


class DataAdaptor(object):
    '''
    classdocs
    '''
    def __init__(self, query, start_ind, end_ind):
        self.query = " ".join(re.sub('[^0-9a-zA-Z]',' ', query).split())
        self.start_ind = start_ind 
        self.end_ind = end_ind # 1<= start_ind <= end_ind <= totalitem. #returned = end_ind-start_ind+1
        self.totalitem = sys.maxint
        self.paper_list = self.getPaper() # [paperEntry]
        self.paper_dict = self.list2dict(self.paper_list) # { ms_id: paperEntry }
    
    def getPaper(self):
        '''using ms query to populate self.paper_list'''
        appId = "0105e2bf-fb9d-4492-a4a4-871671f8e6eb"
        result_id = self.start_ind
        paper_list = []
        first_time = True
        internet_indicator = internet_on()
        while result_id <= min(self.totalitem, self.end_ind): # if end_ind > totalitem, you'll not get as many papers
            ss = result_id
            ee = min(min(self.end_ind, ss+99), self.totalitem) # can only get 100 results maximum at a time
            request = '''http://academic.research.microsoft.com/json.svc/search?AppId=%s&FullTextQuery=%s&ResultObjects=Publication&PublicationContent=AllInfo&StartIdx=%d&EndIdx=%d''' % (appId, self.query, ss, ee)
            print request
            if internet_indicator == True:
                try:
                    response = simplejson.loads(urllib.urlopen(request).read())
                except JSONDecodeError, e:
                    print "%r" % e
                    print "using cached data..."
                    tmp_json = open('ps2/data/tmp_json') # tmp_json is cached for "software engineering" query
                    response = simplejson.load(tmp_json) # convert from json to dict
                    response['d']['Publication']['Result'] = response['d']['Publication']['Result'][0:(ee-ss+1)]
                    tmp_json.close()

            if internet_indicator == False or response['d']['Publication']['Result'] == None: # for debugging purposes when MAS is down
                print "using cached data..."
                tmp_json = open('ps2/data/tmp_json') # tmp_json is cached for "software engineering" query
                response = simplejson.load(tmp_json) # convert from json to dict
                response['d']['Publication']['Result'] = response['d']['Publication']['Result'][0:(ee-ss+1)]
                tmp_json.close()
            if first_time:
                self.totalitem = int(response['d']['Publication']['TotalItem'])
                first_time = False
            for result in response['d']['Publication']['Result']:
                author_list = []
                author_id_list = []
                kws = '' # keywords
                for author in result['Author']:
                    author_list.append(author['FirstName']+' '+author['MiddleName']+' '+author['LastName'])
                    author_id_list.append(author['ID']) # this is string
#                 if author_list == [] or author_id_list == []:
#                     author_list.append('Not Available')
#                     author_id_list.append(-1)
                for kw in result['Keyword']:
                    kws += kw['Name'] + '; '
                try:
                    conf = result['Conference']['FullName']
                    v_id = int(result['Conference']['ID'])
                except TypeError:
                    conf = ''
                    v_id = 0
                try:
                    jnl = result['Journal']['FullName']
                    v_id = int(result['Journal']['ID'])
                except TypeError:
                    jnl = ''
                try:
                    url = result['FullVersionURL'][0]
                except IndexError:
                    url = ''
                paper_list.append(PaperEntry(result_id, int(result['CitationCount']), int(result['ReferenceCount']), 
                                             int(result['Year']), result['Title'],
                                             result['Abstract'], author_list, author_id_list, int(result['ID']), 
                                             conf, jnl, v_id, kws, url ))
                result_id += 1
        return paper_list
    
    def list2dict(self, paper_list):
        '''not only populate self.paper_dict, but also serve as a util function'''
        paper_dict = {}
        for paper in paper_list:
            paper_dict[paper.ms_id] = paper
        return paper_dict
    
    def dict2list(self, paper_dict):
        '''serve as a util function'''
        paper_list = []
        for paper in paper_dict.values(): paper_list.append(paper)
        return paper_list
    
def test():
    da = DataAdaptor("software engineering", 151015, 152025)
    for paper in da.paper_list:
        paper.printPaper()
    
if __name__=="__main__":         
    test()