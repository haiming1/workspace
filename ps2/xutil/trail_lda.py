from gensim import corpora, models, matutils
from xmodel.DataAdaptor import DataAdaptor
from preprocess import cleanText
import time
import logging # to be able to view the topics in the logging
from fpformat import sci
import numpy as np
logging.basicConfig(format='%(asctime)s : %(levelname)s : %(message)s', level=logging.INFO)

# print '-------------------get initial documents--------------------------------'
# start_time = time.time()
# query = "software engineering" # this should be ignored in the corpus
# da = DataAdaptor(query, 1, 100)
# documents = []
# for paper in da.paper_list:
#     document = paper.title+' '+paper.title+' '+paper.abstract+' '+paper.kws
#     documents.append(document)
# print "# of documents: %d" % len(documents)
# print "Time(s): %s" % (time.time()-start_time)
# 
# print '-------------------preprocess initial documents--------------------------------'
# start_time = time.time()
# # first stemming and remove stop words, punctuation, short words, then convert to list 
# texts = [cleanText(document) for document in documents] # we converted document(s) to text(s)
# print "Time(s): %s" % (time.time()-start_time)

texts = [['human', 'interface', 'computer'],
 ['survey', 'user', 'computer', 'system', 'response', 'time'],
 ['eps', 'user', 'interface', 'system'],
 ['system', 'human', 'system', 'eps'],
 ['user', 'response', 'time'],
 ['trees'],
 ['graph', 'trees'],
 ['graph', 'minors', 'trees'],
 ['graph', 'minors', 'survey']]

print "--------------------dictionary---------------------"
start_time = time.time()
dictionary = corpora.Dictionary(texts)
# print(dictionary)
# print(dictionary.token2id)
print "Time(s): %s" % (time.time()-start_time)

print "--------------------bow_corpus---------------------"
start_time = time.time()
bow_corpus = [dictionary.doc2bow(text) for text in texts] # from texts to bow_corpus, text is a list
# print(bow_corpus)
print "Time(s): %s" % (time.time()-start_time)

print "--------------------tfidf_corpus---------------------"
start_time = time.time()
tfidf = models.TfidfModel(bow_corpus)
tmp = bow_corpus # for some weird reason, we can't apply bow_corpus directly to tfidf model
tfidf_corpus =tfidf[tmp] # from bow_corpus to tfidf_corpus
scipy_csc_matrix = matutils.corpus2csc(tfidf_corpus)
print scipy_csc_matrix.toarray()
print "Time(s): %s" % (time.time()-start_time)

# print "--------------------topic model---------------------"
# start_time = time.time()
# # increase the passes to increase the # of converged documents
# lda_model = models.LdaModel(bow_corpus, id2word=dictionary, num_topics=10, passes=3) # can use bow as input
# # the experiments proves that using tfidf with ldaM is like drawing foot to snake
# # lda_model = models.LdaModel(tfidf_corpus, id2word=dictionary, num_topics=20) # also use tfidf as input
# print "Time(s): %s" % (time.time()-start_time)
# 
# print "--------------------corpus topics---------------------"
# start_time = time.time()
# lda_corpus = lda_model[bow_corpus]
# cnt = 0
# scipy_csc_matrix = matutils.corpus2csc(lda_corpus) # transform to scipy for ML purposes
# # n-th row is the n-th feature, it is easy to add new features
# # row is feature, col is item, not easy to add new items
# # extra_feature_matrix = [[1,2,3,4,5,6,7,8,9,0],[1,2,3,4,5,6,7,8,9,0]]
# extra_feature_matrix = [[0]*100] # 100 items for each feature
# feature_matrix = np.concatenate((scipy_csc_matrix.toarray(), extra_feature_matrix)) # convert to np array
# feature_matrix = map(list, zip(*feature_matrix))
# 
# '''
# ?? bug
# scipy_csc_matrix_t = scipy_csc_matrix.transpose() # row is item, col is feature
# extra_item_matrix = [[0]*10] # 10 features for each item
# feature_matrix = np.concatenate((scipy_csc_matrix.toarray(), extra_item_matrix)) # convert to np array
# '''
# print feature_matrix
# print "Time(s): %s" % (time.time()-start_time)
# 
# print "--------------------corpus clusters---------------------"
# from sklearn.cluster import KMeans
# # treat row as item, col as feature
# kmeans_model = KMeans(n_clusters=3, random_state=1).fit(feature_matrix)
# print kmeans_model.labels_
# # TODO
# # 2. convert between array format to sparse matrix format
# # 3. add new features or samples to both format
# # 4. incorporate the array/sparse formats to various algorithms, check the api
# # 1. use networkx louvain method to get more features
# # 5. ability to visualize
