'''
Created on 2014-03-23
This module includes different tools to get features, normalize, and reduce feature space.
This module also includes a process to use all the functions to get features to be consumed by the classifier.

In order to add a new feature to the feature set, you need to implement the new feature construction function in this module, and 
call the function in ps2/ps2/views.py/Get_feature_Thread/run(). And add the new features to get_feature_set function to incorporate these
features into the feature space. Please follow other feauture construction examples, they are pretty similar.
@author: whm
'''
from gensim import corpora, models, matutils
import logging
from matplotlib.pyplot import savefig, close
from random import random
from time import time

import community
import matplotlib.pyplot as plt
import networkx as nx
import numpy as np
from porter2 import stem
from xmodel.DataAdaptor import DataAdaptor
from xutil.clf import Classifier
from xutil.preprocess import cleanText



def construct_lda(all_paper_texts, ignore_string_list=[]):
    '''
    construct lda model from all_paper_texts, stem(word) will be filtered in the dictionary
    '''
    # update dictionary
    try:
        dictionary = corpora.Dictionary.load_from_text('ps2/data/dictionary')
        print "load from text worked"
        dictionary.add_documents(all_paper_texts)
    except IOError:
        dictionary = corpora.Dictionary(all_paper_texts)
        import os
        print os.getcwd()
        print "load from text not work"
    bad_ids = []
    for text in ignore_string_list:
        for word in text.split():
            try: bad_ids.append(dictionary.token2id[stem(word)])
            except KeyError: pass
    dictionary.filter_tokens(bad_ids)
    dictionary.compactify()
    dictionary.save_as_text('ps2/data/dictionary')
    # from preprocessed texts to bow_corpus
    bow_corpus = [dictionary.doc2bow(text) for text in all_paper_texts]
    # train lda model
    lda_model = models.LdaModel(bow_corpus, id2word=dictionary, num_topics=10, passes=4)
    return lda_model

def get_feature_set(lda_model,paper_list, paper_texts, paper_authorID_list, graph):
    '''
    use the lda and/or sna model to get feature_set of paper_list
    paper order in list and texts are supposed to be the same, since we are merging features
    '''
    logging.basicConfig(format='%(asctime)s : %(levelname)s : %(message)s', level=logging.INFO)
    # get lda topic distribution
    dictionary = corpora.Dictionary.load_from_text('ps2/data/dictionary')
    bow_corpus = [dictionary.doc2bow(text) for text in paper_texts]
    lda_corpus = lda_model[bow_corpus] # all query words specific to a task are removed(?early query words may not be removed in later entries, should be no problem, since they are sparse)
    lda_feature_set = matutils.corpus2csc(lda_corpus) # row is feature, column is item
    paper_author_comm = [] # list of the community # of the first author of the paper
    for authorID_list in paper_authorID_list:
        if authorID_list != []: paper_author_comm.append(authorID_list[0]) # paper's author information available
        else: paper_author_comm.append(-1) # use -1 as the place holder for the missing author
    paper_author_comm = [make_partition(paper_author_comm, graph, 'Coauthorship_graph')] # a row of feature
    extra_feature_set = []
    for paper in paper_list:
        # row is item, column is feature
        extra_feature_set.append([paper.v_id, paper.citation_count, paper.reference_count, paper.recency]) 
# http://stackoverflow.com/questions/6473679/python-list-of-lists-transpose-without-zipm-thing
# https://docs.python.org/2/library/functions.html#list
    extra_feature_set = map(list, zip(*extra_feature_set)) # row is feature, column is item
    paper_feature_set = lda_feature_set.toarray()
    paper_feature_set = np.concatenate((paper_feature_set, extra_feature_set)) # row is feature, column is item
    paper_feature_set = np.concatenate((paper_feature_set, paper_author_comm))
    paper_feature_set = map(list, zip(*paper_feature_set)) # row is item, col is feature, classifiers work in that way
    return paper_feature_set

def make_clique(node_list):
    edges = []
    if len(node_list)==1:
        edges.append((node_list[0], node_list[0]))
        return edges
    i = 0
    j = i+1
    while i < len(node_list):
        while j < len(node_list):
            edges.append((node_list[i],node_list[j]))
            j += 1
        i += 1
        j = i+1
    return edges

def make_partition(nodes_to_check, graph, title):
    '''
    This function mines the graph partition using Louvain methods.
    It will save the partition graph.
    It will return the community number from the nodes_to_check
    '''
    #first compute the best partition
    partition = community.best_partition(graph)
    print partition
    print set(partition.values())
    '''
    #drawing
    start = time()
    pos = nx.spring_layout(graph, iterations=100, scale=1)
    print pos
    for com in set(partition.values()) :
        list_nodes = [node for node in partition.keys()
                                    if partition[node] == com]
        com_color = (random(), random(), random())
        nx.draw_networkx_nodes(graph, pos, list_nodes, node_size = 30,
                                    node_color = com_color,
                                    linewidths=0.1, label='comm %d'%com)
    nx.draw_networkx_edges(graph,pos, alpha=0.5)
#     plt.legend()
    plt.title(title)
    annotation = "Node #: %d\nEdge #: %d\nCommunity #: %d" % (graph.number_of_nodes(), graph.number_of_edges(),
                                                                len(set(partition.values())))
    plt.figtext(0.12, -0.04, annotation, horizontalalignment='left')
    savefig('ps2/fig/'+title+'_%.1f.png' % time(), bbox_inches='tight')
#     savefig(title+'_%.1f.png' % time(), bbox_inches='tight')
    close()
    print "drawing graph takes %f seconds" % (time()-start)
    '''
    comm = []
    for node in nodes_to_check:
        if node != -1: # we use -1 as a place holder for a non-existent node
            try:
                comm.append(partition[node]) # there is such node, then we'll return its comm #
            except KeyError:
                print "Exception: This node is not in the graph."
                import sys
    # make the list of comm number for the nodes to check
                sys.exit()
        else: comm.append(-1) # there is no such node, we return -1 as its non-existent comm #
    assert len(comm) == len(nodes_to_check)
    return comm

def process(feature_set):
    '''
    standardize, mean removal, lda, etc.
    depricated
    '''
    return feature_set

def test_make_partition():
    '''
    test of make_partition
    '''
    G = nx.Graph()
    author_list1 = ['Haiming Wang', 'Kenny Wong', 'Mengliao Wang']
    author_list2 = ['Mengliao Wang', 'Liang Huang', 'Kenny Wong']
    author_list3 = ['Jiuyu Sun', 'Yang Liu', 'Hong Zhang'] # when there are only 3 nodes in a com, the coloring will have a little problem: same comm, diff color
    author_list4 = []
    author_list5 = ['Hausi Muller']
    author_list6 = ['a','b','c','d','e','f','g']
    author_list7 = ['1','2']
    G.add_edges_from(make_clique(author_list1))
    G.add_edges_from(make_clique(author_list2))
    G.add_edges_from(make_clique(author_list3))
    G.add_edges_from(make_clique(author_list4))
    G.add_edges_from(make_clique(author_list5))
    G.add_edges_from(make_clique(author_list6))
    G.add_edges_from(make_clique(author_list7))
    G.add_edge(11, 14)
    G.add_edge(11, 16)
    G.add_edge(11, 17)
    nodes_to_check = ['Mengliao Wang', 'Yang Liu', 16, 123]
    print make_partition(nodes_to_check, G, 'Co-authorship Graph')
    
def test_get_feature():
    '''
    depricated
    '''
    print "---------------------Test LDA training-------------------------------"
    logging.basicConfig(format='%(asctime)s : %(levelname)s : %(message)s', level=logging.INFO)
    query = "software engineering"
    da = DataAdaptor(query, 1, 200)
    paper_list = da.paper_list
    paper_texts = [cleanText(paper.title+' '+paper.title+' '
                                  +paper.abstract+' '+paper.kws)
                                  for paper in paper_list]
    lda_model = construct_lda(paper_texts, [query]) # construct lda model
    
    print "---------------------Test feature getting-------------------------------"
    paper_feature_set = get_feature_set(lda_model, paper_list, paper_texts)
    
    print "--------clustering---------"
    from sklearn.cluster import KMeans
    # treat row as item, col as feature
    kmeans_model = KMeans(n_clusters=10, random_state=1).fit(paper_feature_set)
    print kmeans_model.labels_
    
    print "--------classification---------"
    label_list = [] # recent or not
    for paper_feature in paper_feature_set[:-50]:
        if paper_feature[-1] > 25: label_list.append(0) # which is recency
        else: label_list.append(1)
    task = Classifier(paper_feature_set[:-50], label_list)
    test_label_list = task.clf(paper_feature_set[-50:])
    print test_label_list
    for paper_feature in paper_feature_set[-50:]:
        print paper_feature[-1]


if __name__ == "__main__": test_make_partition()