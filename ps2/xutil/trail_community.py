'''
https://networkx.github.io/documentation/latest/reference/generated/networkx.drawing.nx_pylab.draw_networkx_nodes.html?highlight=draw_networkx_nodes#networkx.drawing.nx_pylab.draw_networkx_nodes
http://perso.crans.org/aynaud/communities/index.html#
https://networkx.github.io/documentation/latest/reference/classes.graph.html
This Louvain Method is undirected and unweighted, but is easily extensible to directed adn weighted
'''
from matplotlib.pyplot import savefig
from networkx.classes import graph
from random import random
from time import time

import community
import matplotlib.pyplot as plt
import networkx as nx


#better with karate_graph() as defined in networkx example.
#erdos renyi don't have true community structure
# basic graph:
# https://networkx.github.io/documentation/latest/reference/classes.graph.html
G = nx.Graph()
author_list1 = ['Haiming Wang', 'Kenny Wong', 'Mengliao Wang']
author_list2 = ['Mengliao Wang', 'Liang Huang', 'Kenny Wong']
author_list3 = ['Jiuyu Sun', 'Yang Liu', 'Hong Zhang'] # when there are only 3 nodes in a com, the coloring will have a little problem: same comm, diff color
author_list4 = []
author_list5 = ['Hausi Muller']
author_list6 = ['a','b','c','d','e','f','g']
author_list7 = ['1','2']
def make_clique(node_list):
    edges = []
    if len(node_list)==1:
        edges.append((node_list[0], node_list[0]))
        return edges
    i = 0
    j = i+1
    while i < len(node_list):
        while j < len(node_list):
            edges.append((node_list[i],node_list[j]))
            j += 1
        i += 1
        j = i+1
    return edges
G.add_edges_from(make_clique(author_list1))
G.add_edges_from(make_clique(author_list2))
G.add_edges_from(make_clique(author_list3))
G.add_edges_from(make_clique(author_list4))
G.add_edges_from(make_clique(author_list5))
G.add_edges_from(make_clique(author_list6))
G.add_edges_from(make_clique(author_list7))
G.add_edge(11, 14)
G.add_edge(11, 16)
G.add_edge(11, 17)
G.clear()
G=nx.read_gpickle("2.gpickle")
# G = nx.erdos_renyi_graph(30, 0.05)
# print G

#first compute the best partition
partition = community.best_partition(G)
print partition
print set(partition.values())
#drawing
# pos = nx.circular_layout(G)
# pos = nx.random_layout(G)
# pos = nx.shell_layout(G)
pos = nx.spring_layout(G, iterations=100, scale=1)
# pos = nx.spectral_layout(G)
for com in set(partition.values()) :
    list_nodes = [node for node in partition.keys()
                                if partition[node] == com]
    com_color = (random(), random(), random())
    nx.draw_networkx_nodes(G, pos, list_nodes, node_size = 30,
                                node_color = com_color,
                                linewidths=0.1, label='comm %d'%com)

nx.draw_networkx_edges(G,pos, alpha=0.5)
plt.title('Coauthorship graph')
annotation = "Node #: %d\nEdge #: %d\nCommunity #: %d" % (G.number_of_nodes(), G.number_of_edges(),
                                                                len(set(partition.values())))
plt.figtext(0.12, -0.04, annotation, horizontalalignment='left')
savefig('coauthorship graph', bbox_inches='tight')

plt.show()