import logging
from time import time

def main():
#     logging.info('This is another app')
#     logging.info('Started')
    do_something()
    logging.info('Finished')

def do_something():
    logging.basicConfig(filename='%.f.log'%time(), level=logging.INFO)
    logging.info('Started else')
    logging.info('\n')
    logging.info('\n')
    logging.info('Doing something')
    logging.info('Finished else')

def do_sth_else():
    logging.info('Doing something else')

if __name__ == '__main__':
    main()
