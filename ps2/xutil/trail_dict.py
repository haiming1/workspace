from gensim import corpora

texts = [['human', 'interface', 'computer'],
 ['survey', 'user', 'computer', 'system', 'response', 'time'],
 ['eps', 'user', 'interface', 'system'],
 ['system', 'human', 'system', 'eps'],
 ['user', 'response', 'time'],
 ['trees'],
 ['graph', 'trees'],
 ['graph', 'minors', 'trees'],
 ['graph', 'minors', 'survey']]

dictionary = corpora.Dictionary(texts)
print(dictionary.token2id)

bad_ids = [dictionary.token2id['system'],dictionary.token2id['trees']]
dictionary.filter_tokens(bad_ids)
print(dictionary.token2id)

dictionary.compactify() # to avoid the problem of adding duplicate ids
print(dictionary.token2id)

dictionary.save_as_text('../data/tmp_dictionary')

dictionary2 = corpora.Dictionary.load_from_text('../data/tmp_dictionary')
texts2 = [['human', 'personalization'], ['user']]
dictionary2.add_documents(texts2)
print(dictionary2.token2id)
dictionary2.save_as_text('../data/tmp_dictionary2')