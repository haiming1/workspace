from scipy.sparse import csc_matrix

# http://docs.scipy.org/doc/scipy-0.13.0/reference/generated/scipy.sparse.csc_matrix.html
before = csc_matrix((3,4))
print before.toarray()
print
after = before.transpose() # before does not change, after is the transposed copy
print before.toarray()
print
print after.toarray()