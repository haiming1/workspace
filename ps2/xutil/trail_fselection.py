'''
http://en.wikipedia.org/wiki/Feature_selection
Feature selection techniques provide three main benefits when constructing predictive models:
        improved model interpretability,
        shorter training times,
        enhanced generalisation by reducing overfitting.
It seems only overfitting can help improve ml alg accuracy
'''

from matplotlib.pyplot import savefig
from mpl_toolkits.mplot3d import Axes3D
from random import randint
from sklearn import datasets, preprocessing
from sklearn.decomposition import PCA
from sklearn.ensemble import ExtraTreesClassifier
from sklearn.feature_selection.univariate_selection import SelectPercentile
from sklearn.lda import LDA
from sklearn.svm import LinearSVC

import matplotlib.pyplot as plt
import numpy as np


print(__doc__)

feature_set = [[1050, 17], [1104, 13], [965, 10], [1024, 9], [615, 12], [512, 16], [329, 15], [272, 14], [286, 15], [430, 13], [315, 10], [302, 14], [284, 13], [182, 13], [222, 9], [189, 14], [209, 10], [204, 12], [161, 10], [153, 10], [129, 12], [148, 14], [99, 16], [164, 9], [193, 14], [81, 17], [125, 2014], [96, 13], [129, 12], [103, 10], [144, 7], [68, 14], [109, 11], [66, 16], [117, 7], [97, 10], [81, 12], [84, 13], [64, 11], [102, 12], [75, 13], [80, 12], [54, 15], [75, 13], [47, 17], [64, 7], [43, 14], [75, 2014], [78, 27], [83, 7], [64, 12], [46, 17], [49, 15], [48, 15], [69, 8], [51, 13], [64, 9], [38, 12], [44, 12], [50, 15], [51, 12], [47, 13], [45, 11], [68, 7], [67, 5], [33, 15], [56, 7], [61, 6], [38, 12], [48, 12], [48, 9], [45, 10], [39, 12], [34, 2014], [49, 7], [50, 10], [43, 11], [42, 9], [23, 16], [32, 14], [46, 10], [47, 12], [52, 9], [37, 9], [27, 16], [41, 7], [36, 10], [49, 10], [50, 9], [38, 13], [39, 9], [49, 10], [32, 9], [39, 9], [44, 11], [17, 17], [35, 13], [47, 6], [20, 13], [29, 6]]
for item in feature_set:
    # generate 3 random features
    tmp = 0
    while(tmp < 3):
        item.append(randint(1, 100))
        tmp += 1
label_list = []
# set the label_list using some rules
for entry in feature_set:
    if entry[0] > 100 and entry[1] <= 15:
        label_list.append(1)
    else: label_list.append(0)
print "----------feature label-------------"
print label_list

X = np.array(feature_set)
# feature selection doesn't need to preprocess
# X = preprocessing.StandardScaler().fit(X).transform(X)
y = np.array(label_list)
print "----------original feature shape-------------"
print X.shape

pca = PCA()
X_r = pca.fit(X).transform(X)
print "----------pca shape-------------"
print X_r.shape

# http://scikit-learn.org/stable/auto_examples/decomposition/plot_pca_vs_lda.html#example-decomposition-plot-pca-vs-lda-py
# http://scikit-learn.org/stable/modules/generated/sklearn.lda.LDA.html#sklearn.lda.LDA
# http://scikit-learn.org/stable/modules/lda_qda.html
lda = LDA(n_components=5)  # will only get 2 dimensions still
X_r2 = lda.fit(X, y).transform(X)
print "----------lda shape-------------"
print X_r2.shape

# http://scikit-learn.org/stable/modules/generated/sklearn.ensemble.ExtraTreesClassifier.html#sklearn.ensemble.ExtraTreesClassifier
clf = ExtraTreesClassifier()
X_r3 = clf.fit(X, y).transform(X, "median")
print "----------tree shape-------------"
print X_r3.shape

# http://scikit-learn.org/stable/modules/generated/sklearn.feature_selection.SelectPercentile.html#sklearn.feature_selection.SelectPercentile
# http://scikit-learn.org/stable/modules/feature_selection.html#feature-selection
# Also try: SelectKBest SelectFpr/Fdr/Fwe GenericUnivariateSelect
selector = SelectPercentile(percentile=20)
X_r4 = selector.fit(X, y).transform(X)
print "----------selectpercentile shape-------------"
print X_r4.shape

row = 2
col = 2
ii = 0
# All coefficient eliminated when the labels are not linear, not practical
# clf = LinearSVC(C=0.01, penalty="l1", dual=False)
# X_r4 = clf.fit(X, y).transform(X)
# print "----------l1linearSVC shape-------------"
# print X_r4.shape


print('explained variance ratio (first two components): %s'
      % str(pca.explained_variance_ratio_))

ii += 1
plt.subplot(row, col, ii)
for c, i, pref in zip("rg", [0, 1], ["unlike", "like"]):
    plt.scatter(X[y == i, 0], X[y == i, 1], c=c, label=pref)
plt.legend()
plt.title('Original dataset')

# i += 1
# plt.subplot(row,col,i)
# for c, i, pref in zip("rg", [0, 1], ["unlike", "like"]):
#     plt.scatter(X_r[y == i, 0], X_r[y == i, 0], c=c, label=pref)
# plt.legend()
# plt.title('PCA')

ii += 1
plt.subplot(row, col, ii)
for c, i, pref in zip("rg", [0, 1], ["unlike", "like"]):
    # x-axis is the first dimen of X_r2 whose corresponding class is i
    plt.scatter(X_r2[y == i, 0], X_r2[y == i, 1], c=c, label=pref)  # the usage of np.arrays
plt.legend()
plt.title('LDA')

ii += 1
plt.subplot(row, col, ii)
for c, i, pref in zip("rg", [0, 1], ["unlike", "like"]):
    # x-axis is the first dimen of X_r2 whose corresponding class is i
    plt.scatter(X_r3[y == i, 0], X_r3[y == i, 1], c=c, label=pref)  # the usage of np.arrays
plt.legend()
plt.title('Tree-based')

ii += 1
plt.subplot(row, col, ii)
for c, i, pref in zip("rg", [0, 1], ["unlike", "like"]):
    # x-axis is the first dimen of X_r2 whose corresponding class is i
    plt.scatter(X_r4[y == i, 0], X_r4[y == i, 0], c=c, label=pref)  # the usage of np.arrays
plt.legend()
plt.title('Select percentile')


savefig('Comparison.png', bbox_inches='tight')
# plt.show()
