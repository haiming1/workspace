'''
We test the Standardization, or mean removal and variance scaling of sci-learn
http://scikit-learn.org/stable/modules/preprocessing.html
'''

from sklearn import preprocessing
import numpy as np
X = np.array([[ 0., 0.,  1998, 500], # n items m features, n*m, n=3 and m=4
              [ 0.01,  0.4,  2001, 100],
              [ 0.9,  0, 0, 3000]])
# scale will make different features in the same scale, and for each feature, the mean is 0
# and the std is 1, which is the assumption of several ML alg.
X_scaled = preprocessing.scale(X)
print X_scaled                                          
print X_scaled.mean(axis=0) # mean of each feature(by column)
print X_scaled.std(axis=0) # standard deviation of each feature(by column)
print "-------------------------------------------"
# another way of doing so
# the same scaler can be applied to other data
scaler = preprocessing.StandardScaler().fit(X)
print scaler
print scaler.mean_                                      
print scaler.std_                                       
print scaler.transform(X)
print scaler.transform([[-1.,  1., 0., 1]])
print scaler.transform(scaler.transform([[-1.,  1., 0., 1]]))
print "-------------------------------------------"
# set min or max, check the link above