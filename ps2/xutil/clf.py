'''
Created on 2014-03-21
Using existing features to train classifier.
Using trained classifier to classify the testing data.
* some of the algorithms, e.g. classifiers may not have been implemented, only the interface is there for reference

if you want to add new classifiers to this module, please first implement it inside the Classifier as what other classifiers did (use
cross validation to find the parameters). Also, have it called in select_clfier() function as other classifiers did, and that's it!
@author: whm
'''

from matplotlib.pyplot import savefig, close
from random import randint
from sklearn import neighbors, svm, tree, preprocessing
from sklearn.cross_validation import StratifiedKFold
from sklearn.ensemble.forest import ExtraTreesClassifier
from sklearn.grid_search import GridSearchCV, RandomizedSearchCV
from sklearn.lda import LDA
from sklearn.linear_model import SGDClassifier, LogisticRegression
from sklearn.naive_bayes import GaussianNB
from time import time

import matplotlib.pyplot as plt
import numpy as np


# a list of candidate classifiers
# 1. add as many clfiers as appropriate # done
# 2. use grid search to tune each clfier's parameters # done
# 3. compare different clfiers' score and choose the best one # done
# 4. add preprocessing and dimension reduction before training the clfiers, 
#    preprocessing may reside in feature preparation # done
# 5. return the pipelined processors # done
# ? feature union? visualize clf or plot the results? # done
class Classifier(object):
    '''
    classdocs
    '''
    def __init__(self, feature_set, label_list, label_name_list=['class0', 'class1','class2'], clfier_name = 'unamed clfier', notes=''):
        '''
        Constructor
        '''
        self.notes = notes
        self.clfier_name = clfier_name
        self.label_name_list = label_name_list
        self.label_list = label_list
        self.feature_set = feature_set
        self.reducer = self.dimen_reduce() # used to reduce dimension, comes before preprocessing
        self.feature_set = self.reducer.transform(self.feature_set)
        self.scaler = self.preprocess() # used to scale test data
        self.feature_set = self.scaler.transform(self.feature_set)
        self.kfold = 3
        # http://scikit-learn.org/stable/modules/model_evaluation.html#scoring-parameter
        # TODO:
        # whether choose accuracy or choose precision
        self.scoring = 'accuracy' # candidate: accuracy, recall, precision
        self.skf = StratifiedKFold(self.label_list, self.kfold)
        self.clfier = self.select_clfier()
    
    def preprocess(self):
        '''
        Preprocessing of data, e.g. mean removal, there will also be a preprocessing in the feature preparation module
        http://scikit-learn.org/stable/modules/preprocessing.html
        we only do standardization:
            zero mean, unit variance, same scale
        not normalization:
            we are not comparing the similarity of different items, thus an item doesn't need unit norm
        Preprocessing actually makes training faster
        '''
        scaler = preprocessing.StandardScaler().fit(self.feature_set)
        return scaler

    def dimen_reduce(self):
        '''
        Dimensionality reduction, general methods and specific methods
        1. http://scikit-learn.org/stable/modules/feature_selection.html#feature-selection
            uni/k variate reduction, rank by feature importance, maybe by recursion, need to specify k, not practical
                tree based is more useful, since we can just say, feature_importance>median
                uni/k variate reduction is good as well
            L1LinearSVC can serve as dimen_reducer since they have fit_transform function that can
                sometimes not working
        2. http://scikit-learn.org/stable/modules/decomposition.html#principal-component-analysis-pca
            pca, factor analysis, not taking advantage of labels, not for us
            lda, good example: http://scikit-learn.org/stable/auto_examples/decomposition/plot_pca_vs_lda.html#example-decomposition-plot-pca-vs-lda-py
                components(max_dimension need to be specified)
        '''
        # Done
        # change to tree based? at least compare against tree-based dimen reduction
        # we use lda for feature selection, see trail_fselection for ref
        # http://scikit-learn.org/stable/modules/generated/sklearn.ensemble.ExtraTreesClassifier.html#sklearn.ensemble.ExtraTreesClassifier.transform
#         reducer = LDA(n_components=5)  # will only get 2 dimensions still
        reducer = ExtraTreesClassifier()
        reducer.fit(self.feature_set, self.label_list) # the transform function uses "mean as default"
        return reducer

    def select_clfier(self):
        '''
        Among all the available classifiers, we choose the one with the highest accuracy.
        ensemble and gaussian process are not included yet, thus we will not include and compare them 
        '''
        clfiers = []
        clfiers.append(self.clfier_svm())
        clfiers.append(self.clfier_tree())
        clfiers.append(self.clfier_nb())
        clfiers.append(self.clfier_sgd())
        clfiers.append(self.clfier_lr())
        clfiers.append(self.clfier_nn())
        best_clfier = clfiers[0]
        for clfier in clfiers[1:]:
            if clfier.best_score_ > best_clfier.best_score_: best_clfier = clfier
        print "---------------best classifier--------------------------"
        print best_clfier.best_estimator_
        return best_clfier
    
    def clfier_svm(self):
        '''
        http://scikit-learn.org/stable/modules/svm.html
        1.2.5 
        pro:
            effective in high dimensional space
            effective when dimension > sample size
            memory efficient, because it only uses some support vectors
            versatile: different kernal functions can be specified for decision functions, both linear and non-linear kernels possible
            uses one-against-one/one-against-rest approach for multi-class classification
        con:
            poor when dimension >> sample size
        tips:
            set the kernel cache size higher
            svm is not scale invariant, need to preprocess beforehand
            use GridSearchCV to choose parameters and kernels
        '''
        '''
        http://scikit-learn.org/stable/modules/cross_validation.html
        3.1.2.2
        http://scikit-learn.org/stable/modules/grid_search.html
        3.2.1(note the example: cv parameter can receive cross_validation model) 3.2.2
        http://scikit-learn.org/stable/modules/pipeline.html
            all estimators except last one in a pipeline must be transformers, last one can be transformer or classifier
            3.3.1
            pipeline has all the methods that the last estimator in the pipeline has
            3.4
        http://scikit-learn.org/stable/modules/model_evaluation.html
            
        doing cross validation and finding good parameters:
        evaluating estimator performance: C, kernel and gamma for support vector machine
        '''
        start = time()
        tuned_parameters = [{'kernel': ['rbf'], 'gamma': [1e-3, 1e-4], 'C': [1, 10, 100, 1000]},
                            {'kernel': ['linear'], 'C': [1, 10, 100, 1000]}]
        clfier = GridSearchCV(svm.SVC(cache_size=500), tuned_parameters, cv=self.skf, n_jobs=-1, 
                              scoring=self.scoring)
        # the parameters must be dict for randomized search
#        clfier = RandomizedSearchCV(svm.SVC(), tuned_parameters, n_iter = 10, cv=skf, n_jobs=-1) 
        clfier = clfier.fit(self.feature_set, self.label_list)
        print "-------------------------------------------"
        print "Training svm clfier takes %.2f seconds" % (time()-start)
        print("Best parameters set found on development set:")
        print(clfier.best_params_)
        print("Best score:")
        print(clfier.best_score_)
        return clfier
    
    def clfier_tree(self):
        '''
        decision trees
        http://scikit-learn.org/stable/modules/tree.html
        * 1.8.1  1.8.4  1.8.5
        pro:
            * requires little data preparation(no preprocessing needed)
            * able to handle both numerical and categorical data
            * white box model: results explainable
            * support both  binary and multiclass classification
            * can be visualized
        con:
            * doesn't support missing values
            * may create over complex model that doesn't generalise the data well
            * optimal tree is NPC
            * may create biased trees if some classes dorminate
            
        tips:
            * get the right number ratio of samples to features, or overfitting may occur
            * try performing dimensionality reduction beforehand, e.g. PCA, ICA, feature selection
            * try visualize using export
            * use max_depth and leaf to avoid overfitting
        '''
        # tune and find the best parameters
        # http://scikit-learn.org/stable/modules/generated/sklearn.tree.DecisionTreeClassifier.html#sklearn.tree.DecisionTreeClassifier
        start = time()
        tuned_parameters = [{}]
        clfier = GridSearchCV(tree.DecisionTreeClassifier(), tuned_parameters, cv=self.skf, n_jobs=-1, 
                              scoring=self.scoring)
        # the parameters must be dict for randomized search
#        clfier = RandomizedSearchCV(svm.SVC(), tuned_parameters, n_iter = 10, cv=skf, n_jobs=-1) 
        clfier = clfier.fit(self.feature_set, self.label_list)
        print "-------------------------------------------"
        print "Training decision tree clfier takes %.2f seconds" % (time()-start)
        print("Best parameters set found on development set:")
        print(clfier.best_params_)
        print("Best score:")
        print(clfier.best_score_)
        return clfier

    def clfier_nb(self):
        '''
        naive bayes
        http://scikit-learn.org/stable/modules/naive_bayes.html
        pro:
            simple assumption but works well
            extremely fast compared to more sophisticated ones
        con:
            just a decent classifier
        '''
        # this is a Gaussian NB example: the feature distribution is assumed to be Gaussian.
        start = time()
        tuned_parameters = [{}]
        clfier = GridSearchCV(GaussianNB(), tuned_parameters, cv=self.skf, n_jobs=-1, 
                              scoring=self.scoring)
        # the parameters must be dict for randomized search
#        clfier = RandomizedSearchCV(svm.SVC(), tuned_parameters, n_iter = 10, cv=skf, n_jobs=-1) 
        clfier = clfier.fit(self.feature_set, self.label_list)
        print "-------------------------------------------"
        print "Training naive bayes clfier takes %.2f seconds" % (time()-start)
        print("Best parameters set found on development set:")
        print(clfier.best_params_)
        print("Best score:")
        print(clfier.best_score_)
        return clfier
        
    def clfier_sgd(self):
        '''
        stochastic gradient descent: linear model
        http://scikit-learn.org/stable/modules/sgd.html
        loss function: linear svm, logistic regression
        1.3.1 1.3.5
        pro:
            1. simple yet efficient
            2. often encountered in NLT and text classification
            3. ease of implementation
        con:
            1. sensitive to feature scaling
            2. requires lots of hyper parameters
        multiclass: one versus all
        tips:
            1. scale your data to [0,1] first, if already in [0, 1], then there's no need
            2. find a good alpha in 10.0**-np.arange(1,7) using GridSearch
            3. find a reasonable n, # of iteration
        '''
        start = time()
        tuned_parameters = [{'loss':['hinge', 'log', 'modified_huber', 'squared_hinge', 'perceptron'],
                             'penalty':['l2', 'l1', 'elasticnet']}]
        clfier = GridSearchCV(SGDClassifier(), tuned_parameters, cv=self.skf, n_jobs=-1, 
                              scoring=self.scoring)
        # the parameters must be dict for randomized search
#        clfier = RandomizedSearchCV(svm.SVC(), tuned_parameters, n_iter = 10, cv=skf, n_jobs=-1) 
        clfier = clfier.fit(self.feature_set, self.label_list)
        print "-------------------------------------------"
        print "Training stochastic gradient descent clfier takes %.2f seconds" % (time()-start)
        print("Best parameters set found on development set:")
        print(clfier.best_params_)
        print("Best score:")
        print(clfier.best_score_)
        return clfier

    def clfier_lr(self):
        '''
        generalized linear model
        http://scikit-learn.org/stable/modules/linear_model.html
        mostly regression, but logistic regression is used for classification
        1.1.10
        '''
        start = time()
        tuned_parameters = [{}]
        clfier = GridSearchCV(LogisticRegression(C=1.0, penalty='l1', tol=1e-6), tuned_parameters, 
                              cv=self.skf, n_jobs=-1, scoring=self.scoring)
        # the parameters must be dict for randomized search
#        clfier = RandomizedSearchCV(svm.SVC(), tuned_parameters, n_iter = 10, cv=skf, n_jobs=-1) 
        clfier = clfier.fit(self.feature_set, self.label_list)
        print "-------------------------------------------"
        print "Training logistic regression clfier takes %.2f seconds" % (time()-start)
        print("Best parameters set found on development set:")
        print(clfier.best_params_)
        print("Best score:")
        print(clfier.best_score_)
        return clfier

    def clfier_nn(self):
        '''
        nearest neighbors
        http://scikit-learn.org/stable/modules/neighbors.html
        1.4.2 1.4.5
        '''
        start = time()
        tuned_parameters = [{'n_neighbors':[3, 5], 'algorithm':['auto'], 'weights':['uniform', 'distance']}]
        clfier = GridSearchCV(neighbors.KNeighborsClassifier(), tuned_parameters, 
                              cv=self.skf, n_jobs=-1, scoring=self.scoring)
        # the parameters must be dict for randomized search
#        clfier = RandomizedSearchCV(svm.SVC(), tuned_parameters, n_iter = 10, cv=skf, n_jobs=-1) 
        clfier = clfier.fit(self.feature_set, self.label_list)
        print "-------------------------------------------"
        print "Training nearest neighbors clfier takes %.2f seconds" % (time()-start)
        print("Best parameters set found on development set:")
        print(clfier.best_params_)
        print("Best score:")
        print(clfier.best_score_)
        return clfier
    
    def clfier_ensemble(self):
        '''
        ensemble method
        http://scikit-learn.org/stable/modules/ensemble.html
        combine the predictions of several base estimators to reduce bias
        uses feature selection by weighing feature importance intrinsically, e.g. averaging methods
        1. bagging method: works well with complex models, e.g. fully developed decision tree
        2. boosting method: works well with weak models, e.g. shallow decision tree, weighed majority vote of weak models
        3. averaging method: forests of randomized trees
        '''

    def clfier_gp(self):
        '''
        gaussian process
        http://scikit-learn.org/stable/modules/gaussian_process.html
        classification is just post processing of regression
        result is probabilistic
        1.5.1
        '''

    def clf(self, test_feature_set):
        test_feature_set_new = self.scaler.transform(self.reducer.transform(test_feature_set))
        test_label_list = self.clfier.predict(test_feature_set_new)
        annotation = 'Query:%s \nClassifier:%s \nItem number:%d \nFeature dimension:%d \n' \
                                                % (self.notes,
                                                  self.clfier.best_estimator_, 
                                                  test_feature_set_new.shape[0],
                                                  test_feature_set_new.shape[1])
        draw_fig(test_feature_set_new, test_label_list, self.label_name_list, self.clfier_name, annotation)
        return test_label_list

def draw_fig(feature_set, label_list, label_name_list, title, annotation):
    '''
    we assume there are only two classes
        classifier info
        feature_set shape
    '''
    plt.figure()
    color = "rgb"
    lb = [0,1,2]
    for c, i, pref in zip(color[:len(label_name_list)], lb[:len(label_name_list)], label_name_list):
        if feature_set.shape[1] > 1: 
            plt.scatter(feature_set[label_list == i, 0], feature_set[label_list == i, 1], c=c, label=pref)
        else: 
            plt.scatter(feature_set[label_list == i, 0], feature_set[label_list == i, 0], c=c, label=pref)
        plt.legend()
        plt.title(title)
        # http://matplotlib.org/api/pyplot_api.html?highlight=figtext#matplotlib.pyplot.figtext
        plt.figtext(0.12, -0.2, annotation, horizontalalignment='left')
    savefig('ps2/fig/'+title+'_%.1f.png' % time(), bbox_inches='tight')
#     savefig(title+'_%.1f.png' % time(), bbox_inches='tight')
    close()

def test():
    feature_set = [[1050, 17], [1104, 13], [965, 10], [1024, 9], [615, 12], [512, 16], [329, 15], [272, 14], [286, 15], [430, 13], [315, 10], [302, 14], [284, 13], [182, 13], [222, 9], [189, 14], [209, 10], [204, 12], [161, 10], [153, 10], [129, 12], [148, 14], [99, 16], [164, 9], [193, 14], [81, 17], [125, 2014], [96, 13], [129, 12], [103, 10], [144, 7], [68, 14], [109, 11], [66, 16], [117, 7], [97, 10], [81, 12], [84, 13], [64, 11], [102, 12], [75, 13], [80, 12], [54, 15], [75, 13], [47, 17], [64, 7], [43, 14], [75, 2014], [78, 27], [83, 7], [64, 12], [46, 17], [49, 15], [48, 15], [69, 8], [51, 13], [64, 9], [38, 12], [44, 12], [50, 15], [51, 12], [47, 13], [45, 11], [68, 7], [67, 5], [33, 15], [56, 7], [61, 6], [38, 12], [48, 12], [48, 9], [45, 10], [39, 12], [34, 2014], [49, 7], [50, 10], [43, 11], [42, 9], [23, 16], [32, 14], [46, 10], [47, 12], [52, 9], [37, 9], [27, 16], [41, 7], [36, 10], [49, 10], [50, 9], [38, 13], [39, 9], [49, 10], [32, 9], [39, 9], [44, 11], [17, 17], [35, 13], [47, 6], [20, 13], [29, 6]]
    label_list = []
    # set the label_list using some rules
    for entry in feature_set:
        if entry[0]>40 and entry[1]<=15:
            if entry[0]>100: label_list.append(2)
            else: label_list.append(1)
        else: label_list.append(0)
    print label_list
    test_feature_set = [[1051, 9], [1104, 9], [965, 11], [102, 90], [70, 9], [51, 5], 
                        [20, 30], [60, 20], [50, 20]]
    for item in feature_set:
        # generate 3 random features
        tmp=0
        while(tmp<3):
            item.append(randint(1,100))
            tmp +=1
    for item in test_feature_set:
        tmp=0
        while(tmp<3):
            item.append(randint(1,100))
            tmp +=1
    print feature_set
    print test_feature_set
    task = Classifier(feature_set, label_list, ['like','unsure','dislike'], 'another clfier')
    test_label_list = task.clf(test_feature_set)
    print "Expecting: [2 2 2 0 1 1 0 1 0]"
    print "Result: " 
    print test_label_list # expect [1 1 0 0 1 1]

if __name__ == "__main__": test()

'''
A good reference:
http://nullege.com/codes/show/src@p@y@pyksc-HEAD@src@scripts@learn_base.py/38/sklearn.svm.sparse.LinearSVC
you can't select the best estimators, you can only choose the best parametor for an estimator
but this code shows you a better way of doing so
'''