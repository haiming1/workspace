# http://stackoverflow.com/questions/3764291/checking-network-connection
# http://stackoverflow.com/questions/2712524/handling-urllib2s-timeout-python

import simplejson
from simplejson.decoder import JSONDecodeError
import socket
import urllib
import urllib2


def internet_on():
    try:
        print "..........checking Internet availability.........."
        urllib2.urlopen('http://74.125.228.100', timeout=10)
        print "internet ok..."
        return True
    except urllib2.URLError as err:
        print "%r" % err
    except socket.timeout, e:
        print "%r" % e
    print "internet failure..."
    return False

if __name__ == "__main__": internet_on()