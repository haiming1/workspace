# Haiming Wang
# Ualberta
# haiming1@ualberta.ca
# June 2, 2013

# Input: a text file
# Output: updated text file, whose stopwords and stemming are removed. Output format is in GibbsLDA input file format.
# Note: I used the python nltk lib. The stopwords are the default in nltk, the stemming are porter stemming method

import string
from porter2 import stem
from nltk.stem import WordNetLemmatizer
import re

STOPWORDS = ['i', 'me', 'my', 'myself', 'we', 'our', 'ours', 'ourselves', 'you', 'your', 'yours', 'yourself', 'yourselves', 'he', 'him', 'his', 'himself', 'she', 'her', 'hers', 'herself', 'it', 'its', 'itself', 'they', 'them', 'their', 'theirs', 'themselves', 'what', 'which', 'who', 'whom', 'this', 'that', 'these', 'those', 'am', 'is', 'are', 'was', 'were', 'be', 'been', 'being', 'have', 'has', 'had', 'having', 'do', 'does', 'did', 'doing', 'a', 'an', 'the', 'and', 'but', 'if', 'or', 'because', 'as', 'until', 'while', 'of', 'at', 'by', 'for', 'with', 'about', 'against', 'between', 'into', 'through', 'during', 'before', 'after', 'above', 'below', 'to', 'from', 'up', 'down', 'in', 'out', 'on', 'off', 'over', 'under', 'again', 'further', 'then', 'once', 'here', 'there', 'when', 'where', 'why', 'how', 'all', 'any', 'both', 'each', 'few', 'more', 'most', 'other', 'some', 'such', 'no', 'nor', 'not', 'only', 'own', 'same', 'so', 'than', 'too', 'very', 's', 't', 'can', 'will', 'just', 'don', 'should', 'now', 'abstract']

def is_not_punct(char):
    if char in string.punctuation: return False # punctuation like !"#$%&\'()*+,-./:;<=>?@[\\]^_`{|}~
    else: return True

# Input: text in string form
# Output: purified text
# Rules:    non-alphabetical -> '' (exceptions: 'c++' -> 'cplusplus', '[' or ']' -> ' ')
#            all letters are lower cased, stemming removed, and stopwords removed 
def cleanText(text_string):
#     wnl = WordNetLemmatizer() # we use lemmatizer instead of porter stemmer
    if text_string == '': return []
    # split original text by non-alphanumerical char
    text_tokenized = re.findall(r"\w+",text_string.lower()) 
    # remove stop words and retain only words length >= 3
    # stemming is good for getting root and train, but for decreased readability
    # ignore_list_lmt = [wnl.lemmatize(word) for word in ignore.split()]
#     text_rmv_stpwords = [word for word in text_tokenized if word not in STOPWORDS and len(word) >= 3]
#     text_stem = [wnl.lemmatize(word) for word in text_rmv_stpwords if wnl.lemmatize(word) not in ignore_list_lmt] # stemming
    text_processed = [stem(word) for word in text_tokenized
                      if word not in STOPWORDS]
    return text_processed

def test():
    txt = "The tutorials are organized as a series of examples that highlight various features of gensim. It is assumed that the reader is familiar with the Python language, has installed gensim and read the introduction."
    print cleanText(txt)
    pass

if __name__ == "__main__":
    test()