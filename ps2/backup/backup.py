def generate_new(): 
    # 14/05/2014 rewrite generate_new function, delete what's not necessary
    # write machine learning functions
    # improve speed
    '''
        based on the total paper list and selected paper list, generate new paper list
    '''
    # TODO:
    # see whether put paper_feature_set calculation here or querying part
    # here: each time generate button is pressed, feature has to be recalculated, slower than simply reordering
    #       wait longer in generating part
    # querying part: need to reorder the feature set
    #       wait longer in querying part
    # TODO:
    # reorder paper on all paper list, not just on current list
    global selected_paper_dict, maxVisitedPage, perpage # stay unchanged
    global paper_dict, paper_list, paper_texts, paper_feature_set # will be updated
    # train classifier with current query set and selected paper set
    [feature_set, label_list] = ProcessFeature.feature_label(paper_list[:maxVisitedPage*perpage], selected_paper_dict)
    print "got feature and label of training set, %d training papers" % len(feature_set)
    print "training feature set:"
    print feature_set
    print "training label list:"
    print label_list

    task = Classifier(feature_set, label_list)
    print "classifier trained"
    '''
    test_feature_set = [[1051, 9], [1104, 9], [965, 11], [1024, 90], [700, 9], [512, 5]]
    test_label_list = task.clf(test_feature_set)
    print test_label_list # expect [1 1 0 0 1 1]
    '''
    test_feature_set = ProcessFeature.feature(paper_list)
    print "test feature set"
    print test_feature_set
    test_label_list = task.clf(test_feature_set)
    print "test label list"
    print test_label_list
    print "query results classified"

    # populate paper_dict and paper_list
    new_paper_dict = {}
    new_paper_list = []
    cnt = 0
    
    while cnt < len(test_label_list):
        paper = paper_list[cnt]
        label = test_label_list[cnt]
        cnt += 1
        if paper.ms_id in selected_paper_dict.keys(): continue # we will not show results already selected, 
                                                               # thus len(new list) + len(selection) = len(old list) = len(test label) = len(test features)
        if label == 1:
            new_paper_list.append(paper)
            new_paper_dict[paper.ms_id] = paper
    print "appended paper labelled 1 to the new list, only %d out of %d" % (len(new_paper_list), len(test_label_list))
    
    cnt = 0
    while cnt < len(test_label_list):
        paper = paper_list[cnt]
        label = test_label_list[cnt]
        cnt += 1
        if paper.ms_id in selected_paper_dict.keys(): continue
        if label == 0:
            new_paper_list.append(paper)
            new_paper_dict[paper.ms_id] = paper
    print "appended results with label 0 to new list, new list length %d" % len(new_paper_list)
    
    paper_list = new_paper_list
    paper_dict = new_paper_dict
    
    print "updated global list with new values"
    
    return


def feature_label(paper_list, selected_paper_dict):
    feature_set = [] # 2D, n x m, where n is #paper, m is #feature
    label_list = []
    # add feature and label info of paper_list first
    for paper in paper_list:
        feature_set.append(getPaperFeature(paper))
        if paper.ms_id in selected_paper_dict.keys(): 
            label_list.append(1)
            selected_paper_dict[paper.ms_id].occured = True
        else: label_list.append(0)
    # add feature and label info of rest of selected_paper_dict second
    # reset occured of paper in selected_paper_dict to false
    for msid, paper in selected_paper_dict.items():
        if paper.occured == False:
            feature_set.append(getPaperFeature(paper))
            label_list.append(1)
        else: paper.occured = False # need to set to false since maybe it's referential sending, it go to the next automatically so no worries
    return [feature_set, label_list]

def feature(paper_list):
    feature_set = []
    for paper in paper_list:
        feature_set.append(getPaperFeature(paper))
    return feature_set

def getPaperFeature(paper):
    '''get the paper specific feature'''
    return [paper.recency]
